#!/usr/bin/env bash
set -ex


#BRANCH="$(git symbolic-ref HEAD 2>/dev/null)" ||
#BRANCH="(unnamed branch)"     # detached HEAD

#BRANCH=${BRANCH##refs/heads/}
BRANCH=${BITBUCKET_BRANCH}
function tag {
    tag=$1
    git tag -a ${tag} -m ${tag} && git push origin ${tag}
}

if  [ "x${BRANCH}" == "x" ]; then
    echo "branch not set, dont know what to do. exiting..."
    exit 1
fi


if  [[ ${BRANCH:?must be set} =~ release/.+ ]]; then
    release=$(echo "${BRANCH}"|sed 's"^release/\(.*\)"\1"g'|sed 's"/"-"g')
    export VERSION=${release}-${BITBUCKET_BUILD_NUMBER?:must be set}.RELEASE
else
    export VERSION=dev-${BITBUCKET_BUILD_NUMBER}
fi

echo "building version: ${VERSION}"

version=${VERSION:?"must not be empty!"}

if [ "x${DEBUG}" != "xyes" ]; then
    tag ${version}
fi

